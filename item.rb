class Item
  attr_accessor :product, :quantity

  def initialize(product)
    @product  = product
    @quantity = 1
  end
end
