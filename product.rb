class Product

  attr_accessor :code, :name, :price

  def initialize(code, name, price)
    @code   = code
    @name   = name
    @price  = price
  end

end

@item1 = Product.new('ult_small', 'Unlimited 1 GB', 24.90)
@item2 = Product.new('ult_medium', 'Unlimited 2 GB', 29.90)
@item3 = Product.new('ult_large', 'Unlimited 5 GB', 44.90)
@item4 = Product.new('1gb', '1 GB Data-pack', 9.90)
