require_relative 'shopping_cart'

shopping_cart = ShoppingCart.new

# Add item to shopping_cart
shopping_cart.add(@item1)
shopping_cart.add(@item4, promo_code: 'I<3AMAYSIM')

# Items
puts "Expected Cart Items: "
shopping_cart.list_items

# Total price
puts "Expected Cart Total: "
puts "$#{shopping_cart.total}"

