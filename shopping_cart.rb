require_relative 'item'
require_relative 'product'

class ShoppingCart

  attr_accessor :items

  def initialize
    @items = []
  end

  def add(item, promo_code: nil)
    cart_item = self.items.index{ |i| i.product == item }
    cart_item.nil? ? @items << Item.new(item) : self.items[cart_item].quantity += 1

    verify_promo_code(promo_code)
    add_1gb_data_pack if item.code.eql?('ult_medium')
  end

  def total
    result = 0
    @items.map{ |item| result += pricing_rules_per_item(item) }
    result = result - (result * 0.10) if @promo_code
    '%.2f' % result
  end

  def list_items
    @items.map{ |item| puts "#{item.quantity} X #{item.product.name}" }
  end

  private

  def pricing_rules_per_item(item)
    send("pricing_rules_for_#{item.product.code}", item)
  end

  def pricing_rules_for_ult_small(item)
    total_price = 0
    item_quantity = item.quantity

    while item_quantity > 0
      if item_quantity == 3
        total_price += item.product.price.to_f * 2
        item_quantity = item_quantity - 3
      else
        total_price += item.product.price.to_f
        item_quantity = item_quantity - 1
      end
    end
    total_price
  end

  def pricing_rules_for_ult_medium(item)
    item.product.price.to_f * item.quantity
  end

  def pricing_rules_for_ult_large(item)
    product_price = item.quantity > 3 ? 39.90 : item.product.price
    product_price.to_f * item.quantity
  end

  def pricing_rules_for_1gb(item)
    item.product.price * item.quantity
  end

  def pricing_rules_for_1gb_free(item)
    item.product.price * item.quantity
  end

  def add_1gb_data_pack
    @item5 = Product.new('1gb_free', '1 GB Data-pack', 0.00) if @item5.nil?
    add(@item5)
  end

  def verify_promo_code(promo_code)
    @promo_code = promo_code.to_s.eql?('I<3AMAYSIM') ? true : false
  end

end
